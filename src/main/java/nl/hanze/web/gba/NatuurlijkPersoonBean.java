package nl.hanze.web.gba;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import nl.hanze.web.gba.dao.NatuurlijkPersoonDAO;
import nl.hanze.web.gba.domain.NatuurlijkPersoon;

@Stateless
public class NatuurlijkPersoonBean
{
	// DAO for looking up a NatuurlijkPersoon entry.
	@Inject
	private NatuurlijkPersoonDAO natuurlijkPersoonDAO;

	public NatuurlijkPersoon getNatuurlijkPersoonbyBSN(long bsn)
	{
		return natuurlijkPersoonDAO.getNatuurlijkPersoonbyBSN(bsn);
	}

	@Transactional
	public void add(NatuurlijkPersoon np)
	{
		natuurlijkPersoonDAO.add(np);
	}

}
