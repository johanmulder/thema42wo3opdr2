package nl.hanze.web.gba.dao;

import nl.hanze.web.gba.domain.NatuurlijkPersoon;

/**
 * NatuurlijkPersoonDAO interface.
 * @author Johan Mulder
 *
 */
public interface NatuurlijkPersoonDAO
{
	/**
	 * Get a "natuurlijk persoon" by bsn.
	 * @param bsn
	 * @return
	 */
	public NatuurlijkPersoon getNatuurlijkPersoonbyBSN(long bsn);
	
	/**
	 * Add a "natuurlijk persoon".
	 * @param np
	 */
	public void add(NatuurlijkPersoon np);
	
}
