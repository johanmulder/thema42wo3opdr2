package nl.hanze.web.gba.dao.impl;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import nl.hanze.web.gba.dao.NatuurlijkPersoonDAO;
import nl.hanze.web.gba.domain.NatuurlijkPersoon;

@ApplicationScoped
public class NatuurlijkPersoonDAOImpl implements NatuurlijkPersoonDAO
{
	@PersistenceContext(unitName = "GBA")
	private EntityManager em;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * nl.hanze.web.gba.dao.NatuurlijkPersoonDAO#getNatuurlijkPersoonbyBSN(long)
	 */
	public NatuurlijkPersoon getNatuurlijkPersoonbyBSN(long bsn)
	{
		return em.find(NatuurlijkPersoon.class, bsn);
	}

	@Override
	public void add(NatuurlijkPersoon np)
	{
		em.persist(np);
	}

}
