package nl.hanze.web.gba;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.hanze.web.gba.domain.NatuurlijkPersoon;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

@SuppressWarnings("serial")
public class GBA extends HttpServlet
{
	// NatuurlijkPersoon EJB
	@EJB
	private NatuurlijkPersoonBean npBean;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		if ("get".equals(getAction(request)))
		{
			// Look up the BSN by the BSN from the query string.
			NatuurlijkPersoon np = npBean.getNatuurlijkPersoonbyBSN(getBsn(request));

			// Return json format
			request.setAttribute("np", np);
			RequestDispatcher view = request.getRequestDispatcher("get.jsp");
			view.forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		try
		{
			// Create the new NatuurlijkPersoon.
			NatuurlijkPersoon np = getNatuurlijkPersoonFromPostData(request.getInputStream());
			npBean.add(np);
			// Respond with a http status 201 and the location of the newly
			// created NP
			response.setStatus(HttpServletResponse.SC_CREATED);
			response.addHeader("Location",
					request.getRequestURL() + "?action=get&bsn=" + np.getBsn());
		}
		catch (Exception e)
		{
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			System.err.println("Exception caught: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private NatuurlijkPersoon getNatuurlijkPersoonFromPostData(InputStream is)
	{
		// Convert he posted JSON to a NatuurlijkPersoon.
		JsonReader reader = new JsonReader(new InputStreamReader(is));
		NatuurlijkPersoon np = new Gson().fromJson(reader, NatuurlijkPersoon.class);
		return np;
	}

	/**
	 * Get the action from the query string.
	 * 
	 * @param request
	 * @return
	 */
	private String getAction(HttpServletRequest request)
	{
		return request.getParameter("action");
	}

	/**
	 * Get the BSN from the query string.
	 * 
	 * @param request
	 * @return -1 if bsn is not given in the query string, the long value
	 *         otherwise.
	 */
	private long getBsn(HttpServletRequest request)
	{
		String param = request.getParameter("bsn");
		if (param == null)
			return -1;
		return Long.parseLong(param);
	}
}
